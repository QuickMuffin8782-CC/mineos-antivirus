--[[
╔════════════════════════╗
║ SafeScanner v1.0.0     ║
╚════════════════════════╝

This installer installs the main files for MineOS.

You can create a shortcut to the application directory /MineOS/Applications/SafeScanner.app/ to create a shortcut.

The OS is created by IgorTimofeev and this application can protect it! You can also check out the main gitlab page:
https://gitlab.com/QuickMuffin8782-CC/SafeScanner/

]]
